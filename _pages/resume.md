---
layout: page
title: Resume
---

## EXPERIENCE

### Software Developer II 

##### Total Quality Logistics - Milford, OH
##### January 2018 - Present

Full Stack Developer in charge of creating and maintaining applications, as well as mentoring others and creating/updating team standards.  Mentoring practices include Job Shadowing, Code Reviews, and Onboarding/Training.


### IT CAP Software Developer

##### Total Quality Logistics - Milford, OH
##### January 2016 - January 2018

Full Stack Developer in charge of creating and maintaining accounting and general back office applications using the .NET Framework.  Applications include RESTful Web APIs, Web UIs, Windows Services, Fat Clients, and SQL Jobs, as well as the SQL Stored Procedures/Data Structures and Cloud Architecture that drives them.  IT Career Acceleration Program helped introduce business practices and behaviors.


### “Build Your Own PC” Sales Associate 

##### MicroCenter - Sharonville, OH
##### March 2014 - December 2015

Sales Representative in charge of internal computer components and peripherals.  This includes complete system builds, computer diagnostics, and hardware solutions.

### CSR 

##### MicroCenter - Sharonville, OH
##### Nov 2012 - March 2014

Customer Service Representative in charge of cashiering, returns, restocking, and service diagnostics, check-ins, pickups.

## SKILLS

### Programming

**C#/VB:** RESTful Web APIs, WIndows Services, and Fat Clients  
**SQL:** Data Structures, Stored Procedures, Messaging Queues, and Jobs  
**HTML/CSS/PHP/TypeScript/Javascript (Angular, JQuery):** Website Front-End development and maintenance  
**Python:** Testing, Scripting, and Web Development using the Django Framework  
**Java:** Game Development using the LWJGL Framework  


### Responsibilities

**Event Driven Architecture:** Lead the design of EDA using SQL Service Broker and Azure Service Bus  
**File Converter Service:** Redesign a unified service for all document/image conversions  
**Source Control, CI/CD:** Integrate TFS/VSTS Source Control and CI/CD pipelines into our team standards  
**API Framework:** Develop an extensible API framework to increase development throughput while decreasing bugs  


### Systems

**Source Control:** TFVC, Git (GitLab)  
**Cloud:** Azure (Service Bus, App Insights, Functions)  
**Windows:**  XP-10, Server 2008-2016  
**Linux:** Debian  
**OpenBSD:** PFSense  


## EDUCATION

### Butler Tech, Programming & Software Development

_2010-2013_

Two and a half year of program teaching Java, HTML, CSS, Javascript, PHP, and VB



